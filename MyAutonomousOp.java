package org.firstinspires.ftc.teamcode;

// import android.app.Activity;
// import android.view.View;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
// import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

@Autonomous(name="MyAutonomousOp", group="Linear OpMode")
// @Disabled
public class MyAutonomousOp extends LinearOpMode {
    private DcMotor leftFront = null;
    private DcMotor rightFront = null;
    private DcMotor leftBack = null;
    private DcMotor rightBack = null;
    private Servo jewelArm = null;
    private ColorSensor allianceColorSensor;
    private ColorSensor jewelColorSensor;

    private static final double FORWARD_SPEED = 0.5;
    private static final double REVERSE_SPEED = 0.5;
    private static final double MULTIPLIER_SM = 0.5;
    private static final double MULTIPLIER_LG = 1.5;
    private static final int DURATION_SHORT = 1000;
    private static final int DURATION_LONG = 1250;
    private static double multiplier = 0.0;
    private static int duration = 0;
    private ElapsedTime driveTime = new ElapsedTime();
    private static final double DEPLOY_JEWEL_ARM = 1.0;
    private static final double RAISE_JEWEL_ARM = 0.3;
    private String allianceColor = "none";
    private String jewelColor = "none";

    // public static HardwareMap hardwareMap;
    // float hsvValues[] = {0F, 0F, 0F};
    // final float values[] = hsvValues;
    // final double SCALE_FACTOR = 255;
    // int relativeLayoutId = hardwareMap.appContext.getResources().getIdentifier(
    //     "RelativeLayout", "id", hardwareMap.appContext.getPackageName());
    // final View relativeLayout = ((Activity) hardwareMap.appContext).findViewById(relativeLayoutId);

    @Override
    public void runOpMode() throws InterruptedException {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

       /* Initialize standard Hardware interfaces */
        leftFront = hardwareMap.get(DcMotor.class, "leftFront");
        rightFront = hardwareMap.get(DcMotor.class, "rightFront");
        leftBack = hardwareMap.get(DcMotor.class, "leftBack");
        rightBack = hardwareMap.get(DcMotor.class, "rightBack");
        jewelArm = hardwareMap.get(Servo.class, "jewel_arm");
        allianceColorSensor = hardwareMap.get(ColorSensor.class, "alliance_color");
        jewelColorSensor = hardwareMap.get(ColorSensor.class, "jewel_color");

        // Most robots need the motor on one side to be reversed to drive forward
        // Reverse the motor that runs backwards when connected directly to the battery
        leftFront.setDirection(DcMotor.Direction.FORWARD);
        rightFront.setDirection(DcMotor.Direction.REVERSE);
        leftBack.setDirection(DcMotor.Direction.FORWARD);
        rightBack.setDirection(DcMotor.Direction.REVERSE);

        // Set all motors to zero power
        leftFront.setPower(0);
        rightFront.setPower(0);
        leftBack.setPower(0);
        rightBack.setPower(0);

        // Set all motors to run without encoders.
        // May want to use RUN_USING_ENCODERS if encoders are installed.
        leftFront.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightFront.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        leftBack.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightBack.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        // Wait for the game to start (driver presses PLAY)

        waitForStart();

        // STEP 1 - READ STONE COLOR
        int redValue = allianceColorSensor.red();
        int greenValue = allianceColorSensor.green();
        int blueValue = allianceColorSensor.blue();
        boolean isRed = redValue > blueValue && redValue > greenValue;
        boolean isBlue = blueValue > redValue && blueValue > greenValue;
        if (isRed || isBlue) {
            allianceColor = (isBlue) ? "blue" : "red";
        }

        // STEP 2 - DEPLOY THE JEWEL ARM
        jewelColorSensor.enableLed(true);
        jewelArm.setPosition(DEPLOY_JEWEL_ARM);
        sleep(4000);

        // STEP 3 - READ JEWEL COLOR
        int jredValue = jewelColorSensor.red();
        int jgreenValue = jewelColorSensor.green();
        int jblueValue = jewelColorSensor.blue();
        boolean jIsRed = (jredValue > jblueValue) && (jredValue > jgreenValue);
        boolean jIsBlue = (jblueValue > jredValue) && (jblueValue > jgreenValue);
        if (jIsRed || jIsBlue) {
            jewelColor = (jIsBlue) ? "blue" : "red";
        }

        telemetry.addData("Alliance color is ", allianceColor);
        telemetry.addData("Jewel color is ", jewelColor);
        telemetry.update();

        // sleep(1000);

        // STEP 4
        // if either sensor fails to read color - skip this step
        boolean offStone = false;
        if (!(allianceColor.equals("none") || jewelColor.equals("none"))) {
            if (opModeIsActive()) {
                if (allianceColor.equals("red")) {
                    if (jewelColor.equals("red")) {
                        drive(false, 0.5,500);
                    } else {
                        // jewel color = blue
                        drive( true, 0.5,350);
                        offStone = true;
                    }
                } else { // alliance color = blue
                    if (jewelColor.equals("red")) {
                        drive(true, 0.5, 350);
                    } else {
                        offStone = true;
                        drive( false, 0.5, 500);
                    }
                }
            }
        } // endif both color sensors detected color
        // STEP 5
        jewelArm.setPosition(RAISE_JEWEL_ARM);
        // jewelArm.setPosition(0.1);
        sleep(1000);

        // STEP 6 drive toward safe zone
        if (offStone) {
            multiplier = MULTIPLIER_LG;
            duration = DURATION_LONG;
        } else {
            multiplier = MULTIPLIER_SM;
            duration = DURATION_SHORT;
        }
        if (allianceColor == "red") {
            drive( false, multiplier, duration);
        } else {
            drive(true, multiplier, duration);
        }
    }  // end runOpMode Method
    public void drive(boolean driveForward, // towards front of bot
                      double multiplier,         // decimal percentage
                      int duration          // milliseconds
    ) throws InterruptedException {
        if (driveForward) {
            leftFront.setDirection(DcMotor.Direction.REVERSE);
            rightFront.setDirection(DcMotor.Direction.FORWARD);
            leftBack.setDirection(DcMotor.Direction.REVERSE);
            rightBack.setDirection(DcMotor.Direction.FORWARD);
            leftFront.setPower(FORWARD_SPEED * multiplier);
            rightFront.setPower(FORWARD_SPEED * multiplier);
            leftBack.setPower(FORWARD_SPEED * multiplier);
            rightBack.setPower(FORWARD_SPEED * multiplier);
            sleep(duration);
            leftFront.setPower(0);
            rightFront.setPower(0);
            leftBack.setPower(0);
            rightBack.setPower(0);
        } else {
            leftFront.setDirection(DcMotor.Direction.FORWARD);
            rightFront.setDirection(DcMotor.Direction.REVERSE);
            leftBack.setDirection(DcMotor.Direction.FORWARD);
            rightBack.setDirection(DcMotor.Direction.REVERSE);
            leftFront.setPower(REVERSE_SPEED * multiplier);
            rightFront.setPower(REVERSE_SPEED * multiplier);
            leftBack.setPower(REVERSE_SPEED * multiplier);
            rightBack.setPower(REVERSE_SPEED * multiplier);
            sleep(duration);
            leftFront.setPower(0);
            rightFront.setPower(0);
            leftBack.setPower(0);
            rightBack.setPower(0);
        }
    }
}  // end MyAutonomousOp Class