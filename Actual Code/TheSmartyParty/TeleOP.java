/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode.TheSmartyParty;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
// import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;
import static java.lang.Math.abs;

@SuppressWarnings("FieldCanBeLocal")
@TeleOp(name="TeleOP", group="Linear OpMode")
// @Disabled
public class TeleOP extends LinearOpMode {
    // Declare OpMode members.
    private DcMotor leftFront = null;
    private DcMotor rightFront = null;
    private DcMotor leftBack = null;
    private DcMotor rightBack = null;
    private DcMotor platformMotor = null;
    private DcMotor syringeMotor = null;
    private DcMotor linearAcc = null;
    private CRServo cupServo = null;
    private Servo jewelArm = null;
    private Servo valveShutoff = null;
    private double leftFrontPower = 0.0;
    private double rightFrontPower = 0.0;
    private double leftBackPower = 0.0;
    private double rightBackPower = 0.0;
    //    private boolean platformMotorPower = false;
//    private double syringeMotorPower = 0.0;
//    private double linearAccPower = 0.0;
    private final double DRIVE_HIGH_POWER = 0.75;
    private final double DRIVE_LOW_POWER = 0.25;
    private double drivePowerFactor = 0.5;

    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        // Initialize the hardware variables. Note that the strings used here as parameters
        // to 'get' must correspond to the names assigned during the robot configuration
        // step (using the FTC Robot Controller app on the phone).
        leftFront = hardwareMap.get(DcMotor.class, "leftFront");
        rightFront = hardwareMap.get(DcMotor.class, "rightFront");
        leftBack = hardwareMap.get(DcMotor.class, "leftBack");
        rightBack = hardwareMap.get(DcMotor.class, "rightBack");

        platformMotor = hardwareMap.get(DcMotor.class, "platform_motor");
        syringeMotor = hardwareMap.get(DcMotor.class, "syringe_motor");
        linearAcc = hardwareMap.get(DcMotor.class, "linear_acc");
        cupServo = hardwareMap.get(CRServo.class, "cup_servo");
        valveShutoff = hardwareMap.get(Servo.class, "valve_shutoff");
        jewelArm = hardwareMap.get(Servo.class, "jewel_arm");

        // Most robots need the motor on one side to be reversed to drive forward
        // Reverse the motor that runs backwards when connected directly to the battery
        leftFront.setDirection(DcMotor.Direction.FORWARD);
        rightFront.setDirection(DcMotor.Direction.REVERSE);
        leftBack.setDirection(DcMotor.Direction.FORWARD);
        rightBack.setDirection(DcMotor.Direction.REVERSE);


        // syringeMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        // syringeMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        // syringeMotor.setTargetPosition(1440);


        // Wait for the game to start (driver presses START+A)
        waitForStart();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {
            // https://www.vexforum.com/index.php/7873-programming-mecanum-wheels/0
            // prevent excess power to motor
            drivePowerFactor = (gamepad1.left_bumper ? DRIVE_LOW_POWER : DRIVE_HIGH_POWER);
            // Setup a variable for each drive wheel to save power level for telemetry
            // Left Stick X
            double x1 = Range.clip(-gamepad1.left_stick_x, -1, 1) * drivePowerFactor;
            // Left Stick Y
            double y1 = Range.clip(gamepad1.left_stick_y, -1, 1) * drivePowerFactor;
            // Right Stick X
            double x2 = Range.clip(-gamepad1.right_stick_x, -1, 1) * drivePowerFactor;
            // Right Stick Y
            // double y2 = Range.clip(gamepad1.right_stick_y, -1,1) * drivePowerFactor;
            // throttle = Range.clip(gamepad2.left_stick_y, -1, 1) * 0.5;

            leftFrontPower = y1 + x2 + x1;
            leftBackPower = y1 + x2 - x1;
            rightFrontPower = y1 - x2 - x1;
            rightBackPower = y1 - x2 + x1;

            leftFront.setPower(abs(leftFrontPower) > .1 ? leftFrontPower : 0);
            rightFront.setPower(abs(rightFrontPower) > .1 ? rightFrontPower : 0);
            leftBack.setPower(abs(leftBackPower) > .1 ? leftBackPower : 0);
            rightBack.setPower(abs(rightBackPower) > .1 ? rightBackPower : 0);

            if (gamepad2.dpad_up || gamepad2.dpad_down) {
                platformMotor.setDirection(DcMotor.Direction.FORWARD);
                if (gamepad2.dpad_up) {
                    platformMotor.setPower(-1);
                } else {
                    platformMotor.setPower(1);
                }
            } else {
                platformMotor.setPower(0);
            }

            if (gamepad2.left_bumper || gamepad2.right_bumper) {
                linearAcc.setDirection(DcMotor.Direction.REVERSE);
                if (gamepad2.left_bumper) {
                    linearAcc.setPower(1);
                } else {
                    linearAcc.setPower(-1);
                }
            } else {
                linearAcc.setPower(0);
            }

            if (gamepad2.a || gamepad2.b) {
                cupServo.setDirection(CRServo.Direction.FORWARD);
                if (gamepad2.b) {
                    cupServo.setPower(1.0);
                    cupServo.setDirection(CRServo.Direction.FORWARD);
                } else {
                    cupServo.setPower(-1.0);
                    cupServo.setDirection(CRServo.Direction.REVERSE);
                }
            }

            if ((gamepad2.left_trigger > 0) || (gamepad2.right_trigger > 0)) {
                syringeMotor.setDirection(DcMotor.Direction.FORWARD);
                if (gamepad2.left_trigger > 0) {
                    // syringeMotor.setTargetPosition(1);
                    syringeMotor.setPower(1);
                    valveShutoff.setPosition(1); // open to release vacuum
                    // sleep(2000);
                    // valveShutoff.setPosition(0.5); // close shutoff for next pickup
                } else {
                    // syringeMotor.setTargetPosition(-1);
                    syringeMotor.setPower(-1);
                }
            } else {
                syringeMotor.setPower(0);
                valveShutoff.setPosition(0.5); // close shutoff for next pickup
            }

            if (gamepad2.x) {
                valveShutoff.setPosition(1); // open to release vacuum
                sleep(1000);
                valveShutoff.setPosition(0.5); // close shutoff for next pickup
            }
            if (gamepad2.y) {
                valveShutoff.setPosition(0.5);
            }
            // adding control to raise jewelArm if it falls
            if (gamepad1.y) {
                jewelArm.setPosition(0);
            }
            if (gamepad1.x){
                jewelArm.setPosition(1);
            }

        }
    }
}

