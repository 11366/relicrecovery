package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

@Autonomous(name="AutonomousBlueNear", group="Linear OpMode")
// @Disabled
public class AutonomousBlueNear extends LinearOpMode {
    private DcMotor leftFront = null;
    private DcMotor rightFront = null;
    private DcMotor leftBack = null;
    private DcMotor rightBack = null;
    private Servo jewelArm;
    private static final double DRIVE_POWER = 0.5;
    private static final double MULTIPLIER_SMALL = 0.5;
    private static final double MULTIPLIER_LARGE = 1;
    private static final int DURATION_SHORT = 1000;
    private static final int DURATION_LONG = 1250;
    private static final double DEPLOY_JEWEL_ARM = 1.0;
    private static final double RAISE_JEWEL_ARM = 0.5;
    private static final String ALLIANCE_COLOR = "blue";
    private static String jewelColor = "none";

    @Override
    public void runOpMode() throws InterruptedException {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

       /* Initialize standard Hardware interfaces */
        leftFront = hardwareMap.get(DcMotor.class, "leftFront");
        rightFront = hardwareMap.get(DcMotor.class, "rightFront");
        leftBack = hardwareMap.get(DcMotor.class, "leftBack");
        rightBack = hardwareMap.get(DcMotor.class, "rightBack");
        jewelArm = hardwareMap.get(Servo.class, "jewel_arm");
        ColorSensor jewelColorSensor = hardwareMap.get(ColorSensor.class, "jewel_color");

        // Most robots need the motor on one side to be reversed to moveBot forward
        // Reverse the motor that runs backwards when connected directly to the battery
        leftFront.setDirection(DcMotor.Direction.FORWARD);
        rightFront.setDirection(DcMotor.Direction.REVERSE);
        leftBack.setDirection(DcMotor.Direction.FORWARD);
        rightBack.setDirection(DcMotor.Direction.REVERSE);

        // Set all motors to zero power
        setDriveSpeed(0.0);

        // Set all motors to run without encoders.
        // May want to use RUN_USING_ENCODERS if encoders are installed.
        leftFront.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightFront.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        leftBack.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightBack.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        // STEP 1
        waitForStart(); // Wait for the game to start (driver presses PLAY)

        // STEP 2 - DEPLOY THE JEWEL ARM
        jewelColorSensor.enableLed(true);
        jewelArm.setPosition(DEPLOY_JEWEL_ARM);
        sleep(4000);

        // STEP 3 - READ JEWEL COLOR
        int jewelRedValue = jewelColorSensor.red();
        int jewelGreenValue = jewelColorSensor.green();
        int jewelBlueValue = jewelColorSensor.blue();
        boolean jIsRed = (jewelRedValue > jewelBlueValue) && (jewelRedValue > jewelGreenValue);
        boolean jIsBlue = (jewelBlueValue > jewelRedValue) && (jewelBlueValue > jewelGreenValue);
        if (jIsRed || jIsBlue) {
            jewelColor = (jIsBlue) ? "blue" : "red";
        }

        telemetry.addData("Alliance color is ", ALLIANCE_COLOR);
        telemetry.addData("Jewel color is ", jewelColor);
        telemetry.update();

        // STEP 4
        // if either sensor fails to read color - skip this step
        boolean offStone = false;
        if ((ALLIANCE_COLOR.equals("none") || jewelColor.equals("none"))) {
            jewelArmRaise();
        } else {
            if (opModeIsActive()) {
                if (jewelColor.equals("red")) {
                    moveBot(true, 0.5,500);
                } else {
                    // jewel color = blue
                    moveBot( false, 0.5,350);
                    offStone = true;
                }
            }
            jewelArmRaise();
        } // endif both color sensors detected color

        // STEP 5 moveBot toward safe zone
        double multiplier;
        int duration;
        if (offStone) {
            multiplier = MULTIPLIER_LARGE;
            duration = DURATION_LONG;
        } else {
            multiplier = MULTIPLIER_SMALL;
            duration = DURATION_SHORT;
        }
        moveBot(true, multiplier, duration);

    }  // end runOpMode Method

    // Methods
    private void jewelArmRaise() throws InterruptedException {
        jewelArm.setPosition(RAISE_JEWEL_ARM);
        double servoPosition = RAISE_JEWEL_ARM;
        while (servoPosition >= 0.0) {
            servoPosition -= 0.1;
            jewelArm.setPosition(servoPosition);
            sleep(100);
        }
        jewelArm.setPosition(0.0);
    }

    private void moveBot(
            boolean driveForward,  // towards front of bot
            double multiplier,     // decimal percentage
            int duration           // milliseconds
    ) throws InterruptedException {
        double speed = DRIVE_POWER * multiplier;
        ElapsedTime driveTime = new ElapsedTime();
        if (driveForward) {
            leftFront.setDirection(DcMotor.Direction.REVERSE);
            rightFront.setDirection(DcMotor.Direction.FORWARD);
            leftBack.setDirection(DcMotor.Direction.REVERSE);
            rightBack.setDirection(DcMotor.Direction.FORWARD);
            setDriveSpeed(speed);
        } else {
            leftFront.setDirection(DcMotor.Direction.FORWARD);
            rightFront.setDirection(DcMotor.Direction.REVERSE);
            leftBack.setDirection(DcMotor.Direction.FORWARD);
            rightBack.setDirection(DcMotor.Direction.REVERSE);
            setDriveSpeed(speed);
        }
        driveTime.reset();
        while (driveTime.milliseconds() < duration) {
            sleep(10);
        }
        setDriveSpeed(0.0);
    }

    private void setDriveSpeed(double speed) throws InterruptedException {
        leftFront.setPower(speed);
        rightFront.setPower(speed);
        leftBack.setPower(speed);
        rightBack.setPower(speed);
    }

}  // end MyAutonomousOp Class