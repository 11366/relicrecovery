/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import android.app.Activity;
import android.view.View;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import static org.firstinspires.ftc.teamcode.MethodsAutonomous.DEPLOY_JEWEL_ARM;

@Autonomous(name="OpModeAutonomous", group="Linear OpMode")
// @Disabled
public class OpModeAutonomous extends LinearOpMode {

    // Declare OpMode members.
    MethodsAutonomous robot = new MethodsAutonomous();
    private ElapsedTime runtime = new ElapsedTime();

    static final double FORWARD_SPEED = 0.5;
    static final double REVERSE_SPEED = -0.5;
    static final double TURN_SPEED = 0.3;

    ColorSensor allianceColorSensor;
    ColorSensor jewelColorSensor;
    public String allianceColor;
    public String jewelColor;

    float hsvValues[] = {0F, 0F, 0F};
    final float values[] = hsvValues;
    final double SCALE_FACTOR = 255;
    int relativeLayoutId = hardwareMap.appContext.getResources().getIdentifier("RelativeLayout", "id", hardwareMap.appContext.getPackageName());
    final View relativeLayout = ((Activity) hardwareMap.appContext).findViewById(relativeLayoutId);

    @Override
    public void runOpMode() {
        robot.init(hardwareMap);
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        runtime.reset();  // reset time counter

        // STEP 1 - READ STONE COLOR
        int redValue = allianceColorSensor.red();
        int greenValue = allianceColorSensor.green();
        int blueValue = allianceColorSensor.blue();
        boolean isRed = redValue > blueValue && redValue > greenValue;
        boolean isBlue = blueValue > redValue && blueValue > greenValue;
        if (isRed || isBlue)
            allianceColor = isRed ? "red" : "blue";
        telemetry.addData("Alliance color is ", allianceColor);


        // STEP 2 - DEPLOY THE JEWEL ARM
        robot.jewelArm.setPosition(DEPLOY_JEWEL_ARM);

        // STEP 3 - READ JEWEL COLOR
        redValue = jewelColorSensor.red();
        blueValue = jewelColorSensor.blue();
        greenValue = jewelColorSensor.green();
        isRed = redValue > blueValue && redValue > greenValue;
        isBlue = blueValue > redValue && blueValue > greenValue;
        if (isRed || isBlue)
            jewelColor = isRed ? "red" : "blue";
        telemetry.addData("Jewel color is ", jewelColor);

        // STEP 4
        while (opModeIsActive() && (runtime.seconds() < 1.0)) {
            if (jewelColor.equals(allianceColor)) {
                if (allianceColor.equals("red")) {
                    robot.leftFront.setPower(REVERSE_SPEED);
                    robot.rightFront.setPower(REVERSE_SPEED);
                    robot.leftBack.setPower(REVERSE_SPEED);
                    robot.rightBack.setPower(REVERSE_SPEED);
                    robot.leftFront.setDirection(DcMotor.Direction.FORWARD);
                    robot.rightFront.setDirection(DcMotor.Direction.REVERSE);
                    robot.leftBack.setDirection(DcMotor.Direction.FORWARD);
                    robot.rightBack.setDirection(DcMotor.Direction.REVERSE);
                } else {
                    robot.leftFront.setDirection(DcMotor.Direction.REVERSE);
                    robot.rightFront.setDirection(DcMotor.Direction.FORWARD);
                    robot.leftBack.setDirection(DcMotor.Direction.REVERSE);
                    robot.rightBack.setDirection(DcMotor.Direction.FORWARD);
                }
            }
        }
    }
}
